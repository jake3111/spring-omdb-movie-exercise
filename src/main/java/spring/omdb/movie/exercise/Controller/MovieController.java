package spring.omdb.movie.exercise.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.omdb.movie.exercise.Domain.Movie;
import spring.omdb.movie.exercise.Service.MovieService;

import java.util.List;
import java.util.Map;

@RestController
public class MovieController {

    @Autowired
    private MovieService movieService;

    @GetMapping("/movies")
    public List<Movie> countWords(@RequestParam String q){
        return movieService.getMessage(q);
    }
}
