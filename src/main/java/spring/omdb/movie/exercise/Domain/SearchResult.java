package spring.omdb.movie.exercise.Domain;

import java.util.List;

public class SearchResult {
    private List<Movie> Search;

    public List<Movie> getSearch() {
        return Search;
    }

    public void setSearch(List<Movie> search) {
        Search = search;
    }
}
