package spring.omdb.movie.exercise.Service;

import com.google.gson.Gson;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import spring.omdb.movie.exercise.Domain.Movie;
import spring.omdb.movie.exercise.Domain.SearchResult;

import java.util.List;

@Service
public class MovieService {
    private final RestTemplate restTemplate = new RestTemplate(); // <------ #2

    public List<Movie> getMessage(String sRequest) {
        StringBuilder requestString = new StringBuilder("http://www.omdbapi.com/?s=");
        requestString.append(sRequest);
        requestString.append("&apikey=e480ebad");
        String response = this.restTemplate.getForObject( // <-// ----- #3
                requestString.toString(),
                String.class
        );
        Gson gson = new Gson();
        SearchResult searchResult = gson.fromJson(response, SearchResult.class);
        return searchResult.getSearch();
    }
}
